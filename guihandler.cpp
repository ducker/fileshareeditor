#include "guihandler.h"
#include <QDebug>
#include <QTextEdit>
#include "clientthread.h"

GUIHandler::GUIHandler(QObject *textArea, QObject *editTextArea, QObject *editButton, QObject *saveButton, QObject *cancelButton, QObject *editRectangle) :
    textArea(textArea), editTextArea(editTextArea), editButton(editButton), saveButton(saveButton), cancelButton(cancelButton), editRectangle(editRectangle)
{
    status = NORMAL;
    QObject::connect(editTextArea, SIGNAL(editTextAreaChangedSignal(QString)), this, SLOT(editTextAreaChangedSlot(QString)));
    QObject::connect(editButton, SIGNAL(editButtonClickedSignal()), this, SLOT(editButtonChangedSlot()));
    QObject::connect(saveButton, SIGNAL(saveButtonClickedSignal()), this, SLOT(saveButtonChangedSlot()));
    QObject::connect(cancelButton, SIGNAL(cancelButtonClickedSignal()), this, SLOT(cancelButtonChangedSlot()));
}

void GUIHandler::editTextAreaChangedSlot(const QString &msg) {

}

void GUIHandler::editButtonChangedSlot() {
    if (editRectangle->property("visible").toBool()) return;
    editTextArea->setProperty("text", getTextAreaSelectedText());
    clientThread->acceptFragment(getTextAreaSelectionBegin(), getTextAreaSelectionEnd());
}

void GUIHandler::saveButtonChangedSlot() {
   clientThread->sendEditedFragment();
}

void GUIHandler::cancelButtonChangedSlot() {
    clientThread->cancelFragmentEditing();
}

void GUIHandler::setTextAreaText(QString txt)
{
    textArea->setProperty("text", txt);
}

void GUIHandler::setClientThread(ClientThread *thread)
{
    clientThread = thread;
}

QString GUIHandler::getTextAreaSelectedText()
{
    return textArea->property("selectedText").toString();
}

QString GUIHandler::getEditedText()
{
    return editTextArea->property("text").toString();
}

unsigned long GUIHandler::getTextAreaSelectionBegin()
{
    return textArea->property("selectionStart").toInt();
}

unsigned long GUIHandler::getTextAreaSelectionEnd()
{
    return textArea->property("selectionEnd").toInt();
}

void GUIHandler::setEditing(bool editing) {
    editRectangle->setProperty("visible", editing);
}
