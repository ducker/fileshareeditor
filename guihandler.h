#ifndef GUIHANDLER_H
#define GUIHANDLER_H

#include <QObject>

enum GUIStatus {
    NORMAL,
    EDITING,
    WAITING
};

class ClientThread;

class GUIHandler: public QObject
{
    Q_OBJECT
public:
    GUIHandler(QObject *textArea, QObject *editTextArea, QObject *editButton, QObject *saveButton, QObject *cancelButton, QObject *editRectangle);
    void setTextAreaText(QString txt);
    void setClientThread(ClientThread *thread);
    QString getEditedText();
    QString getTextAreaSelectedText();
    unsigned long getTextAreaSelectionBegin();
    unsigned long getTextAreaSelectionEnd();
    void setEditing(bool editing);
    GUIStatus status;

signals:
    void chooseFragmentToEditSignal(unsigned long begin, unsigned long end);
    void cancelEditingFragmentSignal();
    void saveEditedFragmentSignal();

public slots:
    void editTextAreaChangedSlot(const QString &msg);
    void editButtonChangedSlot();
    void saveButtonChangedSlot();
    void cancelButtonChangedSlot();

protected:
    QObject *textArea;
    QObject *editTextArea;
    QObject *editButton;
    QObject *saveButton;
    QObject *cancelButton;
    QObject *editRectangle;
    ClientThread *clientThread;
    bool isEditing;
};

#endif // GUIHANDLER_H
