#define _MULTI_THREADED

#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>

#include "clientthread.h"
#include "guihandler.h"


const int OK = 0;
const int WRONG_PASSWORD = 1;
const int SERVER_IS_FULL = 2;
const int ERROR = 3;
const int DENIED = 4;
const int SENDING = 5;
const int UPDATING = 6;

extern "C" void *threadStart(void *);
extern "C" void sigAction(int);

ClientThread* ClientThread::thread;

ClientThread::ClientThread(GUIHandler *gui, char* hostName, int port, char* passwd) : port(port), gui(gui)
{
        id = pthread_self();
        thread = this;

        if (port<1024 || port>65535) {
            fprintf(stderr, "Nr portu musi byc z przedzialu 1024-65535\n");
            ::exit(EXIT_FAILURE);
        }

        if (strlen(passwd)>16) {
                fprintf(stderr, "Maksymalna dlugosc hasla to 16 znakow\n");
                ::exit(EXIT_FAILURE);
        }

        sd = socket(PF_INET, SOCK_STREAM, 0);

        struct hostent* host = gethostbyname(hostName);
        if (host==0) {
            herror("gethostbyname");
                ::exit(EXIT_FAILURE);
        }

        memset(&adr, 0, sizeof(struct sockaddr_in));
        adr.sin_family = AF_INET;
        adr.sin_port = htons(port);
        adr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)host->h_addr)));

        if (connect(sd, (struct sockaddr *)&adr, sizeof(struct sockaddr))) {
            perror("connect");
            close(sd);
            ::exit(EXIT_FAILURE);
        }

        printf("Polaczeno\n");

        write (sd, passwd, strlen(passwd)+1);

        read(sd, &msg, sizeof(int));

        if (msg == WRONG_PASSWORD) {
            printf("Podane haslo jest nieprawidlowe\n");
            close(sd);
            ::exit(EXIT_FAILURE);
        } else if (msg == SERVER_IS_FULL) {
            printf("Serwer pelny\n");
            close(sd);
            ::exit(EXIT_FAILURE);
        }

        printf("Haslo zaakceptowane\n");

        recvTxt();

        signal(SIGUSR1, sigAction);

        pthread_t threadid;
        pthread_create(&threadid, 0, threadStart, this);
}

void ClientThread::setGUIHandler(GUIHandler *handler)
{
    this->gui = handler;
}

void ClientThread::recvTxt() {
    unsigned long len;

    printf("Transfer tekstu:\n");

    read(sd, &len, sizeof(unsigned long));

    printf("  naglowek\n");

    unsigned long pos = 0;
    txt = (char*)malloc(len+1);
    txt[len]='\0';

    printf("  tekst []");

    while(pos!=len) {
        memset(buf, '\0', 1025);
        pos += read(sd, &(txt[pos]), len-pos>=1024 ? 1024 : len-pos);
    }
    printf("%s", txt);
    printf("\n");

    int cnt;
    tab[0] = tab[1] = 0;
    len=0;

    read(sd, &cnt, sizeof(int));
    fflush(stdout);

    if (pthread_self()==id) {
        gui->setTextAreaText(QString(txt));

        for(; cnt; cnt--) {
            read(sd, tab, 2*sizeof(unsigned long));
           // gui.dodajRegion(tab[0], tab[1]);
        }
    } else {
        todo = 1;
        pthread_kill(id, SIGUSR1);
        while (todo);

        printf("  fragmentow w edycji: %d (", cnt);
        for(; cnt; cnt--) {
            read(sd, tab, 2*sizeof(unsigned long));
            printf(" %d %d", tab[0], tab[1]);
            todo = 2;
            pthread_kill(id, SIGUSR1);
            while (todo);
        }

        printf(" )\n");
        todo = 3;
        pthread_kill(id, SIGUSR1);
        while (todo);
    }

    fflush(stdout);
}

void ClientThread::acceptFragment(unsigned long pocz, unsigned long kon) {
    gui->status = WAITING;
    write(sd, "akcp", sizeof(int));
    write(sd, &pocz, sizeof(unsigned long));
    write(sd, &kon, sizeof(unsigned long));
}

void ClientThread::cancelFragmentEditing() {
    write(sd, &DENIED, sizeof(int));
    gui->setEditing(false);
    gui->status = NORMAL;
}

void ClientThread::sendEditedFragment() {
    write(sd, &SENDING, sizeof(int));
    QString txt = gui->getEditedText();
    unsigned long len = txt.length();
    write(sd, &len, sizeof(unsigned long));
    if (writeall(sd, txt.toStdString().c_str(), len) == 0)
        cancelFragmentEditing();
}

int ClientThread::writeall(int s, const char*buf, unsigned long len) {
  int total = 0;
  int bytesleft = len;
  int n;

  while(total < len) {
    n = write(s, &buf[total], bytesleft);
    if (n == -1) { break; }
    total += n;
    bytesleft -= n;
  }

  len = total;

  return n==-1?-1:0;
}

void ClientThread::run() {
    int msg;

    for (;;) {
    if (!read(sd, &msg, sizeof(int))) {
        todo = 5;
        pthread_kill(id, SIGUSR1);
        while(todo);
        close(sd);
        break;
    } else {
        if (msg == OK) {
            todo = 4;
            pthread_kill(id, SIGUSR1);
            while (todo);
            recvTxt();
        } else if (msg == UPDATING) {
            read(sd, &len, sizeof(long));
            todo = 7;
            pthread_kill(id, SIGUSR1);
            while (todo);
            recvTxt();
        } else {
            todo = 6;
            pthread_kill(id, SIGUSR1);
            while(todo);
        }
    }
    }
}

void ClientThread::doWaitingAction() {
    printf("todo: %d\n", todo);
    switch(todo) {
        case 1:
            gui->setTextAreaText(txt);
            break;
        case 2:
            //gui.dodajRegion(tab[0], tab[1]);
            break;
        case 3:
            break;
        case 4:
            if (gui->status == WAITING) {
                gui->setEditing(true);
                gui->status = EDITING;
            }
            break;
        case 5:
            ::exit(EXIT_FAILURE);
            break;
    }
    todo = 0;
}

ClientThread* ClientThread::getWatek() {
    return thread;
}

void *threadStart(void *obiekt) {
    printf("thread start\n");
    ((ClientThread*)obiekt)->run();
}

void sigAction(int) {
    printf("sigAction\n");
    ClientThread::getWatek()->doWaitingAction();
}

