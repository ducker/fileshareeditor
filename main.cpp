#include <QApplication>
#include <QQmlApplicationEngine>
#include "guihandler.h"
#include "clientthread.h"

ClientThread *thread;


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QObject *rootObject = engine.rootObjects().first();
    QObject *textArea = rootObject->findChild<QObject*>("mainTextArea");
    QObject *editRectangle = rootObject->findChild<QObject*>("editRectangle");
    QObject *editTextArea = rootObject->findChild<QObject*>("textAreaEdit");
    QObject *editButton = rootObject->findChild<QObject*>("menuEditButton");
    QObject *saveButton = rootObject->findChild<QObject*>("saveButton");
    QObject *cancelButton = rootObject->findChild<QObject*>("cancelButton");

    GUIHandler guiHandler(textArea, editTextArea, editButton, saveButton, cancelButton, editRectangle);

    char *host = (char *)"localhost";
    int port = 8778;
    char *passwd = (char *)"passwd";

    if (argc >= 3) {
        host = argv[1];
        passwd = argv[3];
        port = QString(argv[2]).toInt();
    }

    ClientThread clientThread(&guiHandler, host, port, passwd);
    thread = &clientThread;
    guiHandler.setClientThread(thread);

    return app.exec();
}
