import QtQuick 2.3
import QtQuick.Controls 1.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                signal editButtonClickedSignal()
                id: editButton
                objectName: "menuEditButton"
                text: qsTr("Edit")
                onTriggered: editButton.editButtonClickedSignal();
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    TextArea {
        signal textAreaChangedSignal(string msg)
        id: textArea
        readOnly: true
        objectName: "mainTextArea"
        text: qsTr("")
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
    }

    Rectangle {
        id: editRectangle
        objectName: "editRectangle"
        visible: false
        width: parent.width
        height: parent.height
        anchors.centerIn: parent

        TextArea {
            signal editTextAreaChangedSignal(string msg)
            id: textAreaEdit
            objectName: "textAreaEdit"
            text: qsTr("")
            width: parent.width
            height: parent.height - 35
            onTextChanged: {
                textAreaEdit.editTextAreaChangedSignal(textArea.text)
            }
        }

        Button {
            signal saveButtonClickedSignal()
            id: saveButton
            objectName: "saveButton"
            x: parent.width/2 - saveButton.width
            y: parent.height - saveButton.height - 5
            text: qsTr("Zapisz")
            onClicked: saveButton.saveButtonClickedSignal();
        }

        Button {
            signal cancelButtonClickedSignal()
            id: cancelButton
            objectName: "cancelButton"
            x: parent.width/2
            y: parent.height - cancelButton.height - 5
            text: qsTr("Anuluj")
            onClicked: cancelButton.cancelButtonClickedSignal();
        }
    }
}
