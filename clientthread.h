#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <netinet/in.h>
#include <qstring.h>

#include <QObject>

class GUIHandler;

class ClientThread
{

public:
    ClientThread(GUIHandler *gui, char* hostName, int port, char* passwd);
    void run();
    void recvTxt();
    void doWaitingAction();
    static ClientThread* getWatek();
    void setGUIHandler(GUIHandler *handler);
    void acceptFragment(unsigned long, unsigned long);
    void cancelFragmentEditing();
    void sendEditedFragment();

private:
    int writeall(int, const char*, unsigned long);
    static ClientThread*thread;

    GUIHandler *gui;

    int sd;
    int port;
    int todo;
    int pos;
    int msg;
    char* txt;
    unsigned long tab[2];
    long len;
    struct sockaddr_in adr;
    char buf[1025];
    pthread_t id;
};

#endif // CLIENTTHREAD_H
